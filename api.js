const searchPokemon = () => {
    const name = document.getElementById("pokemonName").value;
    fetch(`https://pokeapi.co/api/v2/pokemon/${name}`)
        .then(response => response.json())
        .then(data => {
            getCard(data)
            console.log(data);
        })
}

const randomPokemon = () => {
    const id = Math.floor(Math.random() * (150 + 1));
    fetch(`https://pokeapi.co/api/v2/pokemon/${id}`)
        .then(response => response.json())
        .then(data => {
            getCard(data)
            console.log(data);
        })
}


const getCard = async (data) => {
    let cardBody = document.getElementById("card");
    document.getElementById("modalName").innerHTML = `${data.name.toUpperCase()}`;
    document.getElementById("modalBody").innerHTML = `<p>Tipo: ${data.types[0].type.name}</p>
    <p>Altura: ${data.height}</p>
    <p>Peso: ${data.weight} kg</p>`;
    cardBody.innerHTML = `<div class="card" style="width: 18rem;">
    <img src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${data.id}.png" class="card-img-top" alt="...">
    <div class="card-body">
        <h5 class="card-title">${data.name.toUpperCase()}</h5>
        <p class="card-text">Tipo: ${data.types[0].type.name}</p>
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Saber más
        </button>
    </div>
    </div>`
}

const addFav = (data) => {
    `<tr>
    <th scope="row">${data.id}</th>
    <td>${data.name}</td>
    <td>${data.weight}</td>
</tr>`
}